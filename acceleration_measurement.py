import time
import h5py
import adafruit_adxl34x
import numpy as np
import board
import json
import os

from functions.m_operate import prepare_metadata
from functions.m_operate import log_JSON
from functions.m_operate import set_sensor_setting

"""Parameter definition"""
# -------------------------------------------------------------------------------------------#1-start
# TODO: Adjust the parameters to your needs
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
path_setup_json = "datasheets/setup_speaker.json"  # adjust this to the setup json path
measure_duration_in_s = 20

# ---------------------------------------------------------------------------------------------#1-end

"""Prepare Metadata and create H5-File"""
(
    setup_json_dict,
    sensor_settings_dict,
    path_h5_file,
    path_measurement_folder,
) = prepare_metadata(path_setup_json, path_folder_metadata="datasheets")

print("Setup dictionary:")
print(json.dumps(setup_json_dict, indent=2, default=str))
print()
print("Sensor settings dictionary")
print(json.dumps(sensor_settings_dict, indent=2, default=str))
print()
print(f"Path to the measurement data h5 file created: {path_h5_file}")
print(f"Path to the folder in which the measurement is saved: {path_measurement_folder}")


"""Establishing a connection to the acceleration sensor"""
i2c = board.I2C()  # use default SCL and SDA channels of the pi
try:
    accelerometer = adafruit_adxl34x.ADXL345(i2c)
except Exception as error:
    print(
        "Unfortunately, the ADXL345 accelerometer could not be initialized.\n \
           Make sure your sensor is wired correctly by entering the following\n \
           to your pi's terminal: 'i2cdetect -y 1' "
    )
    print(error)


# -------------------------------------------------------------------------------------------#2-start
# TODO: Initialize the data structure
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#erstellen der leeren Listen
a_x = []
a_y = []
a_z = []
t = []
#dict aus leeren listen erstellen
sub_dict = {'acceleration_x': a_x,
            'acceleration_y': a_y,
            'acceleration_z': a_z,
            'timestamp': t}
#UUID aus dict auslesen
UUID = list(setup_json_dict.keys())[2]
#erstellen von dict mit UUID als key für erstes dict
main_dict = {UUID: sub_dict}
#print(main_dict)
# ---------------------------------------------------------------------------------------------#2-end


# -------------------------------------------------------------------------------------------#3-start
# TODO: Measure the probe
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

#unter der Annahme, dass die Reaktionszeit des Pi in guter Näherung als 0 betrachtet werden kann, und aufgrund der großen Menge Aufwand den diese Näherung erspart, nutze ich keine tatsächliche time-function,
#sondern lasse den Pi nach jeder Messung 1ms hochzählen, und glaube fest daran, dass wenn er bei 20s ankommt, auch tatsächlich 20s vergangen sind.

#festlegen von Startzeit als 0 und der Zeitabstände zwischen den Messungen
t_now = 0
timestep = 0.001
#abfrage jede ms
while t_now <= measure_duration_in_s:
    #einlesen der daten vom Sensor
    a = accelerometer.acceleration
    #speichern der daten in vorher erstellten und im dict abgelegten Listen
    a_x.append(a[0])
    a_y.append(a[1])
    a_z.append(a[2])
    t.append(t_now)
    #hochzählen und warten um 1ms
    t_now += timestep
    time.sleep(timestep)

# ---------------------------------------------------------------------------------------------#3-end

# -------------------------------------------------------------------------------------------#4-start
# TODO: Write results in hdf5
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#umwandeln der listen in arrays
a_X = np.array(a_x)
a_Y = np.array(a_y)
a_Z = np.array(a_z)
T = np.array(t)
#öffnen der hdf5
with h5py.File(path_h5_file, "w")as hf:
    #erstellen von RawData
    raw_data =hf.create_group("RawData")
    #erstellen der subgroup
    sensor = raw_data.create_group(UUID)
    #erstellen der datasets
    sensor.create_dataset("acceleration_x", data = a_X)
    sensor.create_dataset("acceleration_y", data = a_Y)
    sensor.create_dataset("acceleration_z", data = a_Z)
    sensor.create_dataset("timestamp", data = T)
    #zuteilen der attrs
    sensor["acceleration_x"].attrs["unit"] = "m/(s^2)"
    sensor["acceleration_y"].attrs["unit"] = "m/(s^2)"
    sensor["acceleration_z"].attrs["unit"] = "m/(s^2)"
    sensor["timestamp"].attrs["unit"] = "s"
# ---------------------------------------------------------------------------------------------#4-end

"""Log JSON metadata"""
log_JSON(setup_json_dict, path_setup_json, path_measurement_folder)
print("Measurement data was saved in {}/".format(path_measurement_folder))
