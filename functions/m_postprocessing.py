"""Functions for processing measurement data"""

import numpy as np
from numpy.fft import fft
from typing import Tuple


def get_vec_accel(x: np.ndarray, y: np.ndarray, z: np.ndarray) -> np.ndarray:
    """Calculates the vector absolute value of the temporal evolution of a vector (x, y, z).

    Args:
        x (ndarray): Vector containing the temporal elements in the first axis direction.
        y (ndarray): Vector containing the temporal elements in the second axis direction.
        z (ndarray): Vector containing the temporal elements in the third axis direction.

    Returns:
        (ndarray): Absolute value of the evolution.
    """
    #definieren leerer Liste für werte und Laufvariable
    value = []
    i = 0
    #abfragen aller Werte durch while-Schleife
    while i < len(x):
        #für jede Stelle der Arrays Berechnen des Betrags und Speichern in Liste
        v = pow( pow(x[i],2) + pow(y[i],2) + pow(z[i],2) ,0.5)
        value.append(v)
        i += 1
    # Liste zu Array machen und ausgeben
    av = np.array(value)   
    return av

def interpolation(time: np.ndarray, data: np.ndarray) -> Tuple[np.ndarray, np.ndarray]:
    """Linearly interpolates values in data.

    Uses linear Newtonian interpolation. The interpolation points are distributed linearly over the
    entire time (min(time) to max(time)).

    Args:
        time (ndarray): Timestamp of the values in data.
        data (ndarray): Values to interpolate.

    Returns:
        (ndarray): Interpolation points based on 'time'.
        (ndarray): Interpolated values based on 'data'.
    """
   


def my_fft(x: np.ndarray, time: np.ndarray) -> Tuple[np.ndarray, np.ndarray]:
    """Calculates the FFT of x using the numpy function fft()

    Args:
        x (ndarray): Measurement data that is transformed into the frequency range.
        time (ndarray): Timestamp of the measurement data.

    Returns:
        (ndarray): Amplitude of the computed FFT spectrum.
        (ndarray): Frequency of the computed FFT spectrum.
    """
    #Mittelwert berechnen und auf 0 anpassen
    m = np.mean(x)
    new_av = x - m

    from numpy.fft import fft, ifft
    #festlegen der Abtastrate und berechnen und ausgeben von Frequenz und Amplitude nach Vorbild des gestellten Codes
    sr = 1000
    X = fft(new_av)
    N = len(new_av)
    n = np.arange(N)
    T = N/sr
    freq = n/T  

    return X, freq
    